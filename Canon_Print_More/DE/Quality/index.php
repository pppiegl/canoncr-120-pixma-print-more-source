<?php
		// --------------------------------------------------------------------------------------------------------------
		// --------------------------------------------------------------------------------------------------------------
		/*
			TITLE:			Wellcom-Creative-Previewer
			VERSION:		2.0
			DATE:			  27-08-2017
			AUTHOR:			Lee Redmond
			NOTES:			Wellcom / BBH. Creative Banner Previewer.

			FEATURES:		1.0 - Background Colour Themes - White/Grey/Black

									2.0 - √ JSON file structure
											- √ Re-orginise modes: HTML5 | FlASH | BACKUP
											- √ simplified mobile CSS
											- √ Download creative feature
											- √ WCP Guide link
											- √ SVG branding routed through JSON Manifest
											- √ Branding Logo called from single asset



		*/

		$sVersion		  			= "2.0";
		$sAWSUrl						= ""; // Absolute path to the main assets
		$wcpRepoURL  				= "/../../wcp/download/wcp.zip";
		$wcpHelpURL  				= "/../../wcp/guidelines/index.html";

		// --------------------------------------------------------------------------------------------------------------

		// JSON
		$data = file_get_contents('creatives.json'); // put the contents of the file into a variable
		$creatives = json_decode($data); // decode the JSON feed


		$agency		  		= (string)$creatives-> agency; 			    // eg. "BBH"; // Set Co-Partner client ie. Default / BBH / Atomic
		$client 			  = (string)$creatives-> client; 			    // eg. "Audi";
		$title 			    = (string)$creatives-> campaign; 			  // eg. "Approved  Used";
		$route			    = (string)$creatives-> route; 			    // eg. "Standard HTML5";
		$clickTag 		  = (string)$creatives-> clicktag; 			  // eg. "http://www.google.com"; // Click Through
		$developer	 	  = (string)$creatives-> developer;		  	// eg. "Lee Redmond"; // name
		$role			      = (string)$creatives-> role	;			      // eg. "HTML5 Animation"; // role
		$swfPrefix 		  = (string)$creatives-> prefix;

		$banners 				= (array)$creatives-> creatives;

		// echo count($banners). "<br/>";;

 		$bannerList  		= array ();

		foreach ($banners as $value) {
				$newCreative = (string)$value;
				array_push ($bannerList, $newCreative);
				// echo $value . "<br/>";
		}



		// --------------------------------------------------------------------------------------------------------------

		// Theme Colour:
		$theme =	 $_GET['theme'];
		if ($theme == "white" || $theme == NULL) $themename = "white";
		if ($theme == "grey" ) $themename = "grey";
		if ($theme == "black" ) $themename = "black";


		// Logo Path
		$logo	 = "https://staging.wellcomww.co.uk/wcp/assets/images/branding/" . $agency . ".svg";
 
		// Exclamation Symbol
		$exclamation	= "/wcp/assets/images/exclamation.svg";


		// --------------------------------------------------------------------------------------------------------------

		// Set Cookie for preview mode ie Flash or Backup:

		$mode =	 $_GET['mode'];
		if ($mode == "html5" || $mode == NULL) $modeValue = "html5";
		if ($mode == "flash") $modeValue = "flash";
		if ($mode == "backup") $modeValue = "backup";

		setcookie("PreviewMode",$modeValue, time()+3600*24);

		// --------------------------------------------------------------------------------------------------------------

		// Get format of the creative:

		$format = $_GET["format"];
		if (!$format) $format = $bannerList[0];
		$dimensions 	= preg_match("/\d{2,}[x]\d{2,}/", $format, $size); // regular expression looking for dimension ie 120x600, 728x90, 300x250, etc
		$xPos 			  = strpos($size[0], "x");
		$swfW 			  = $xPos ? substr($size[0], 0, $xPos) : "300";
		$swfH 			  = $xPos ? substr($size[0], $xPos+1) : "250";

		/*
		echo "<p>format 	= " . $format . "</p>";
		echo "<p>swfW 		= " . $swfW . "</p>";
		echo "<p>swfH 		= " . $swfH . "</p>";
		echo "<p>filename 	= " . $swfPrefix . $format . "</p>";
		*/

		// --------------------------------------------------------------------------------------------------------------

		// check if download.zip exists - if so display the download.zip button
		$filename = 'download.zip';
		$bDownloadExists = false;

		if (file_exists($filename)) {
				$bDownloadExists = true;
		} else {
				$bDownloadExists = false;
		}


		// --------------------------------------------------------------------------------------------------------------

		// Set values for the dropdown:
		$formatLinkStr = "";
		foreach($bannerList as $f)
		{
			if ($f == $format)
			{
				// Current creative
				$backupPrefix	= $swfPrefix . $f; // echo "backupPrefix  = " . $backupPrefix;
				$formatLinkStr .= "<option value='index.php?format=$f&mode=$modeValue&theme=$themename' selected>$f</option>";
			}
			else $formatLinkStr .= "<option value='index.php?format=$f&mode=$modeValue&theme=$themename'>$f</option>";
		}



		// --------------------------------------------------------------------------------------------------------------

		// Check for backup file extension
		$backupGIF 		= $backupPrefix. ".gif";
		$backupJPG 		= $backupPrefix. ".jpg";
		$backupPNG 		= $backupPrefix. ".png";
		$backupFiles	= array ($backupGIF , $backupJPG, $backupPNG);

		// $bBackupExists
		foreach($backupFiles as $filename)
		{
			// echo $filetype;
			if ($data = @getimagesize($filename ) && $filename == $backupPrefix. ".gif" ) {$backupSuffix	= ".gif"; /* echo "its a gif" */ ;}
			if ($data = @getimagesize($filename ) && $filename == $backupPrefix. ".jpg" ) {$backupSuffix	= ".jpg"; /* echo "its a jpg" */ ;}
			if ($data = @getimagesize($filename ) && $filename == $backupPrefix. ".png" ) {$backupSuffix	= ".png"; /* echo "its a png" */ ;}
		}

	?>

<!doctype html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
	<head>
		<title><?php echo $client . " | " . $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

        <!-- CSS -->
				<link rel='stylesheet' type='text/css' href='/wcp/assets/css/default.css' />

				<!-- Font Awesome -->
        <link rel="stylesheet" href="/wcp/assets/fonts/font-awesome/css/font-awesome.min.css">

        <!-- Colour Themes -->
        <?php
						if ($themename == "white") {
							echo "<link rel='stylesheet' type='text/css' href='/wcp/assets/css/default.css' />";
						} else if ($themename == "grey") {
							echo "<link rel='stylesheet' type='text/css' href='/wcp/assets/css/grey.css' />";
						} else {
							echo "<link rel='stylesheet' type='text/css' href='/wcp/assets/css/black.css' />";
						}
					?>





		<style type="text/css">
		</style>

        <!-- JS -->

		<script src="/wcp/assets/js/swfobject.js" type="text/javascript"></script>
        <script type="text/javascript">
			// SWFObject
			//<![CDATA[
			swfobject.embedSWF('<?php echo $swfPrefix, $format, ".swf" ?>','flash','<?php echo $swfW ?>','<?php echo $swfH ?>','9.0.0','','',{flashVars:'clickTag=<?php echo $clickTag ?>'});

			//]]>
		</script>

        <script type="text/javascript">

			var idFooter =	document.getElementById("footer");

			function alertFunc() {
				console.log ("Hello!");
			}

		</script>


	</head>

	<body>

       <!-- HEADER -->

        <div id="header">

						<!-- Branding Logo -->
						<div id='logo'>
						    <!-- grab the raw content of the svg. Now magic happens  -->
						    <?php echo file_get_contents($logo); ?>
						</div>

						<div id="title"><?php echo $client ?></div>
            <div id="desc">
                <span class="info">Campaign: <span class="det"><?php echo $title ?></span>&nbsp;&nbsp;<span class="iota">&Iota;</span>&nbsp;&nbsp;</span>
								<span class="info">Creative: <span class="det"><form id="selector"><select name="dropdown" id="dropdown"><?php echo $formatLinkStr ?></select></form></span>&nbsp;&nbsp;<span class="iota">&Iota;</span>&nbsp;&nbsp;
								<span class="info"><?php echo $role ?>: <span class="det"><?php echo $developer ?></span>&nbsp;&nbsp;<span class="iota">&Iota;</span>&nbsp;&nbsp;</span>
                <span class="info">Route: <span class="det"><?php echo $route ?></span>&nbsp;&nbsp;<span class="iota">&Iota;</span>&nbsp;&nbsp;</span>

				<!-- Get file size of creative -->
				<?php

					 $bannerSWF = $swfPrefix. $format . ".swf";
					 $bannerBackup = $swfPrefix. $format . $backupSuffix;
					 $bannerHTML5	=  $swfPrefix. $format;

					 $nFlashExists = file_exists($bannerSWF);
					 $nBackupExists = file_exists($bannerBackup);
					 $nHTML5Exists = file_exists($swfPrefix. $format);



					if ($modeValue == 'flash' && $nFlashExists == 1) // Is this in Flash mode and does Flash Exist
					{
						$filename	= $swfPrefix. $format . ".swf";
						$fileSize = round (filesize($filename)/1024, 2) . 'kb';
						// $modificationDate	= getModificationDate($filename);
						$modificationDate	= str_replace("-", " ", getModificationDate($filename));
					}
					else if ($modeValue == 'backup' && $nBackupExists == 1 && $backupSuffix != "") // Is this in BAckup mode and does Backup Exist
					{
						$filename	= $swfPrefix. $format . $backupSuffix;
						$fileSize = round (filesize($filename)/1024, 2) . 'kb';
						// $modificationDate	= getModificationDate($filename);
						$modificationDate	= str_replace("-", " ", getModificationDate($filename));

					}
					else if ($modeValue == 'html5' && $nBackupExists == 1) // Does HTML5 exist
					{
						$filename	= $swfPrefix. $format;
						$directorySize = dirsize ($filename);
						$fileSize = round ($directorySize/1024, 2) . 'kb';
						// $modificationDate	= getModificationDate($filename);
						$modificationDate	= str_replace("-", " ", getModificationDate($filename));
					}
					else
					{
						$fileSize = 'n/a'; // No Flash, No Backup or no HTML5! - n/a
						$modificationDate	= "n/a";
					}

					// ------------------------------------------------------------
					// Get Modification Date Stamp
					// ------------------------------------------------------------

					function getModificationDate ($fileName) {
						$modDate = date ("d-M-Y H:i", filemtime($fileName));
						return  $modDate;
					}

					// ------------------------------------------------------------
					// Get Directory Content Size
					// ------------------------------------------------------------

					function dirsize($dir)
					{
					  @$dh = opendir($dir);
					  $size = 0;
					  while ($file = @readdir($dh)) {
							if ($file != "." and $file != "..") {
								  $path = $dir."/".$file;
								  if (is_dir($path)) {
										$size += dirsize($path); // recursive in sub-folders
								  }
								  elseif (is_file($path)) {
										$size += filesize($path); // add file
								  }
							}
					  }
					  @closedir($dh);
					  return $size;
					}

					// ------------------------------------------------------------


					function getUrl() {
					  $url  = @( $_SERVER["HTTPS"] != 'on' ) ? 'http://'.$_SERVER["SERVER_NAME"] :  'https://'.$_SERVER["SERVER_NAME"];
					  $url .= ( $_SERVER["SERVER_PORT"] !== 80 ) ? ":".$_SERVER["SERVER_PORT"] : "";
					  $url .= $_SERVER["REQUEST_URI"];

					  $url = str_replace('&amp;', '&', $url);

					  return $url;
					}


				?>

        <!-- Display Creative Size -->
				<span class="info">Size: <span class="det"><?php echo $fileSize ?></span>&nbsp;&nbsp;<span class="iota">&Iota;&nbsp;&nbsp;</span></span>

        <!-- Display Creative Size -->
				<span class="info">Modified: <span class="det"><?php echo $modificationDate ?></span>&nbsp;&nbsp;</span>

				<!-- Get Modification Date of Creative -->


        <?php
				?>

        </div>

        <script>
					// Drop down menu redirects
					var urlmenu = document.getElementById( 'dropdown' );
					urlmenu.onchange = function() {
						  window.location.assign(this.options[ this.selectedIndex ].value);
					};
				</script>


        </div> <!-- End of #header -->

       <hr />

       <!-- CONTENT -->

       <div id="content">


       <!-- Switch out content-->

       <?php

      // --------------------------------------------------
      // Check for FLASH MODE
      // --------------------------------------------------

		  $swfBanner		= $swfPrefix. $format . ".swf";
		  $swfBackup		= $swfPrefix. $format . $backupSuffix;

		  $bFlashExists 	=  file_exists($swfBanner);
		  $bBackupExists 	=  file_exists($swfBackup);


		  if($modeValue == 'flash'){

				if ($bFlashExists == 1 || $bBackupExists == 1 ) {

					if ($bFlashExists == 1) {
							echo "<div id='flash'>" ;
								if ($bBackupExists == 1) {
									echo "<p><img class='exclamation' src='" . $exclamation . "' alt='!' /></br></br>Oops! Flash plugin is not available.</p>";
									echo "<img src='" .  $swfBackup . "' width='". $swfW ."' height='". $swfH ."' alt='backup' />";
								} else {
									echo "<p><img class='exclamation' src='" .  $exclamation . "' alt='!' /></br></br>Oops! Flash and Backup previews are not available for this creative.</p>";
								}
							echo "</div>";
					}

					elseif ($bBackupExists == 1) {
							echo  "<p><img class='exclamation' src='" . $exclamation . "' alt='!' /></br></br>Oops! Flash preview is not available for this creative.</p>";

							if ($backupSuffix != "") {
								echo "<img src='" . $swfBackup . "' width='". $swfW ."' height='". $swfH ."' alt='backup' />";
							}
					}

				} else
				{
					echo  "<p><img class='exclamation' src='" . $exclamation . "' alt='!' /></br></br>Oops! Flash and Backup previews are not available for this creative.</p>";
				}

		  }

		  // --------------------------------------------------
          // Check for BACKUP MODE
          // --------------------------------------------------

		if($modeValue == 'backup'  ){

			$backupBanner = $swfPrefix. $format . $backupSuffix;

			// echo "$backupSuffix = " + $backupSuffix;
			// echo "format	= " + $swfPrefix;

			if (file_exists($backupBanner) && $backupSuffix != "") {
					echo
					"<div id='backup'>
					<img src='" .  $swfPrefix . $format . $backupSuffix . "' width='". $swfW ."' height='". $swfH ."' alt='backup' />
					</div>";

			 } else {
					echo "<p><img class='exclamation' src='" . $exclamation . "' alt='!' /></br></br>Oops! Backup preview is not available for this creative.</p>";
					/* echo "<p>$backupBanner</p>"; */
			}

		}

		// --------------------------------------------------
        // Check for HTML5 MODE
       	// --------------------------------------------------

		if($modeValue == 'html5'){

			$html5banner		= $swfPrefix. $format;

			if (file_exists($html5banner)) {
					echo "<iframe src='" . $html5banner . "/index.html' align='center' width='". $swfW ."' height='". $swfH ."'></iframe>";


			 } else {
					echo "<p><img class='exclamation' src='" . $exclamation . "' alt='!' /></br></br>Oops! HTML5 preview is not available for this creative.</p>";
					/* echo "<p>$backupBanner</p>"; */
			}

		}

  ?>



       </div> <!-- End of #content -->

       <!-- DEVELOPMENT PANEL -->

         <div id="dev">
       		<p>banner name: <?php echo $swfPrefix. $format.$backupSuffix ?></p>
            <p>width: <?php echo $swfW ?></p>
            <p>height: <?php echo $swfH ?></p>
            <?php

				$filename = $swfBanner;
				$fileSize = round (filesize($filename)/1024, 2);
				echo $fileSize.'kb';

			?>
			</div> <!-- End of Development panel -->


        <hr />

       <!-- FOOTER -->

       <div id="footer">

            <!-- Preview mode -->
            <span id="preview-mode">

						<?php
								if ($modeValue == "html5") echo "<strong>HTML5</strong>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='index.php?format=$format&mode=flash&theme=" . $themename . "'>Flash</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='index.php?format=$format&mode=backup&theme=" . $themename . "'>Backup</a>";
								if ($modeValue == "flash") echo "<a href='index.php?format=$format&mode=html5&theme=" . $themename . "'>HTML5</a>&nbsp;&nbsp;|&nbsp;&nbsp;<strong>Flash</strong>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='index.php?format=$format&mode=backup&theme=" . $themename . "'>Backup</a>";
								if ($modeValue == "backup") echo "<a href='index.php?format=$format&mode=html5&theme=" . $themename . "'>HTML5</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='index.php?format=$format&mode=flash&theme=" . $themename . "'>Flash</a>&nbsp;&nbsp;|&nbsp;&nbsp;<strong>Backup</strong>";
						?>
						</span>

            <!-- Colour theme -->

            <span id="background-theme">
                  <span class="theme" id="theme-white" <?php echo $whiteborder; ?><?php echo 'theme-on' ?>> <?php  echo "<a href='index.php?format=$format&mode=" . $modeValue . "&theme=white' title='white theme'></a>"  ?> </span>
                  <span class="theme" id="theme-grey" <?php echo $greyborder; ?>> <?php  echo "<a href='index.php?format=$format&mode=" . $modeValue . "&theme=grey' title='grey theme'></a>"  ?> </span>
                  <span class="theme" id="theme-black" <?php echo $blackborder; ?>> <?php  echo "<a href='index.php?format=$format&mode=" . $modeValue . "&theme=black' title='black theme'></a>"  ?> </span>

								  <?php

										// Download Link
								 		if ($bDownloadExists == true) {
								 			echo "<span id='download' title='Download Banners'><a href='download.zip'><i class='fa fa-download' aria-hidden='true'></i></a></span>";
								 		}

								  ?>

						</span>

       </div> <!-- End of footer -->

       <!-- WCP Cloud download -->


			 	<div id="wcp">
							<span title="Creative Previer version" id="wcp-version"><?php echo $sVersion; ?></span>
							<span id="wcp-title">Creative Previewer</span>
							<a id="wcp-question" title="User guide" href="<?php echo $wcpHelpURL; ?>"><i class="fa fa-question-circle " aria-hidden="true"></i></a>
							<a id="wcp-cloud" title="Download Creative Previewer" href="<?php echo $wcpRepoURL; ?>"><i class="fa fa-cloud-download " aria-hidden="true"></i></a>

				</div>



    </body>

		<!-- GSAP -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.0/TweenMax.min.js"></script>

		<script>

		// ------------------------------------------------------------------------
		// Hide / Show Animation Tab
		// ------------------------------------------------------------------------

		var vpWidth = window.innerWidth;


		TweenMax.ticker.fps(60)
		TweenMax.ticker.addEventListener("tick", ticker);

		function ticker () {
				vpWidth = window.innerWidth;
				// console.log ("vpWidth = " + vpWidth);
				if ( vpWidth <= 799) { tab.style.display = "none"; }

				if ( vpWidth >= 800 && bTabShow == true) { tab.style.display = "block"; }

		}


			var tab = document.getElementById("wcp");
			tab.style.display = "none";
			var bTabShow = false;

			document.addEventListener('keydown', keycheck, false);


			function keycheck (evt) {
				const keyNum = evt.which;

				if ( keyNum == 72 && bTabShow == false ) {
						// show tab
						tab.style.display = "block";
						bTabShow = true;
				} else if ( keyNum == 72 && bTabShow == true ) {
					// hide tab
					tab.style.display = "none";
					bTabShow = false;
				}

				// Mobile Constraints





				console.log ("keyNum = " + keyNum);
			}

			// ------------------------------------------------------------------------
			// Help Tab andimation controller
			// ------------------------------------------------------------------------


			var bTabOpen = false;
			var tab = document.getElementById("wcp");
			tab.addEventListener("click", checkTab, false);

			var timerID;
			var nExpand = -190; // px the tab expands

			function checkTab () {

				if (bTabOpen == false) {
					bTabOpen = true;
					tabAnim (0);
					timerID = setTimeout(function(){ tabAnim (nExpand); }, 6000);
				} else {
					bTabOpen = false;
					tabAnim (nExpand);
					window.clearTimeout(timerID);
				}
			}


			function tabAnim (nPos) {
				var sPos = nPos + "px";
				TweenMax.to( [tab], 0.3, { css:{right:sPos},  ease:Sine.easeInOut});
			}

		</script>


		<!-- Branding Logo -->
		<?php

					// White / Grey background
					if ($theme == "white" || $theme == "grey" || $theme == NULL) {
						echo "<script> document.getElementById('inverse').style.display = 'none'; </script>";
						echo "<script> document.getElementById('default').style.display = 'block'; </script>";
					}

					// Black background
					if ($theme == "black" ) {
						echo "<script> document.getElementById('inverse').style.display = 'block'; </script>";
						echo "<script> document.getElementById('default').style.display = 'none'; </script>";
					}



		?>


</html>
