// (function (window) {
  window.onload = function() {

  var controller = {


    init: function () {
      this.setVars();
      this.timeline = new TimelineLite();

      var exitBtn = document.getElementById('exitBtn');

      // add exit click event
      this.addEvent(exitBtn, 'click', this.clickHandler);

      // show the container as we are ready to playt animation
      document.getElementById('container').style.display = "block";

      this.startAnimation();
    },

    setVars: function () {
      this.main = document.querySelector('.container');
      // this.frameOne = this.main.querySelector('.frame--one');
      this.frameTwo = this.main.querySelector('.frame--two');
      // this.sceneContent = this.main.querySelector('.scene--2__content');
      this.cta = this.main.querySelector('.scene--2__cta');
      this.printer = this.main.querySelector('.frame--two__printer');

    },

    startAnimation: function () {

      this.timeline.to('.printer-light', 1, { y: 0, delay: 3 })
      this.timeline.from(this.frameTwo, 2.5, { maxHeight: 0, delay: 0 }, "-=0.6")
      this.timeline.to('.scene--2', 0.5, { opacity: 1 }, "-=0.5");
      this.timeline.to('.frame-2-copy', 0.5, { opacity: 1 }, "-=0.5");
      this.timeline.to(this.printer, 1, {y:-35},"-=2.0");
      this.timeline.to(this.cta, 0.5, { opacity: 1 });
    },

    // handles adding events to the passed elements
    addEvent: function (el, clickType, clickHandler) {

      if (el.addEventListener) {
        // newer browsers event listener
        el.addEventListener(clickType, clickHandler, false);
      }
      else {
        // OLDER IE - event listener
        el.attachEvent("onclick", clickHandler);
      }
    },

    clickHandler: function () {
      window.open(clickTag);
    }


  }



  controller.init();
}

// }(window));
